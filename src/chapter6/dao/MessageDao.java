package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Message;
import chapter6.exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("    user_id, ");
            sql.append("    text, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");                  // user_id
            sql.append("    ?, ");                  // text
            sql.append("    CURRENT_TIMESTAMP, ");  // created_date
            sql.append("    CURRENT_TIMESTAMP ");   // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, message.getUserId());
            ps.setString(2, message.getText());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //つぶやきの削除
    public void delete(Connection connection, int messageId) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM messages WHERE id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, messageId);

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //つぶやきの編集
    public Message select(Connection connection, int messageId) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();

            sql.append("SELECT * FROM messages WHERE id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, messageId);

            ResultSet rs = ps.executeQuery();

            List<Message> editmessagestext = toMessages(rs);

            if (editmessagestext.isEmpty()) {
            	return null;
            }else {
            	return editmessagestext.get(0);
            }

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Message> toMessages(ResultSet rs) throws SQLException {

        List<Message> editmessagestext = new ArrayList<Message>();
        try {
            while (rs.next()) {
                Message edittext = new Message();
                edittext.setId(rs.getInt("id"));
                edittext.setText(rs.getString("text"));
                edittext.setUserId(rs.getInt("user_id"));
                edittext.setCreatedDate(rs.getTimestamp("created_date"));
                edittext.setUpdatedDate(rs.getTimestamp("updated_date"));

                editmessagestext.add(edittext);
            }
            return editmessagestext;
        } finally {
            close(rs);
        }
    }

    public void update(Connection connection, Message message) {

    	 PreparedStatement ps = null;
         try {
             StringBuilder sql = new StringBuilder();
             sql.append("UPDATE messages SET ");
             sql.append("text = ? ");
             sql.append("WHERE id = ? ");

             ps = connection.prepareStatement(sql.toString());

             ps.setString(1, message.getText());
             ps.setInt(2, message.getId());

             ps.executeUpdate();

         } catch (SQLException e) {
             throw new SQLRuntimeException(e);
         } finally {
             close(ps);
         }
    }
}