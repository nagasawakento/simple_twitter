package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserComment;
import chapter6.exception.SQLRuntimeException;

public class UserCommentDao {

	public List<UserComment> select(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();

            sql.append("SELECT ");
            sql.append("    comments.id as id, ");
            sql.append("    comments.text as text, ");
            sql.append("    comments.user_id as user_id, ");
            sql.append("    comments.message_id as message_id, ");
            sql.append("    users.account as account, ");
            sql.append("    users.name as name, ");
            sql.append("    comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");

            sql.append("ORDER BY created_date ASC limit " + num);

            ps = connection.prepareStatement(sql.toString());
            ResultSet rs = ps.executeQuery();

            List<UserComment> messages = toUserComment(rs);
            return messages;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserComment(ResultSet rs) throws SQLException {

        List<UserComment> messages = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
            	UserComment message = new UserComment();
                message.setId(rs.getInt("id"));
                message.setText(rs.getString("text"));
                message.setUserId(rs.getInt("user_id"));
                message.setMessageId(rs.getInt("message_id"));
                message.setAccount(rs.getString("account"));
                message.setName(rs.getString("name"));
                message.setCreatedDate(rs.getTimestamp("created_date"));

                messages.add(message);
            }
            return messages;
        } finally {
            close(rs);
        }
    }
}
