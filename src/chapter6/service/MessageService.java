package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

//実践課題②
import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //実践課題②

    public List<UserMessage> select(String userId, String start, String end) {
        final int LIMIT_NUM = 1000;

        Connection connection = null;
        try {
            connection = getConnection();

            //実践課題②

            Integer id = null;
            if(!StringUtils.isEmpty(userId)) {
              id = Integer.parseInt(userId);
            }

            //つぶやきの絞り込み

            if(!StringUtils.isEmpty(start)) {
            	start += " 00:00:00";
            }else {
            	start = "2021/10/00 00:00:00";
            }

            if(!StringUtils.isEmpty(end)) {
            	end += " 23:59:59";
            }else {

            	Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                end = sdf.format(date);
            }

            List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, start, end);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //つぶやきの削除
    public void delete(int messageId) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().delete(connection, messageId);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //つぶやきの編集
    public Message select(int messageId) {

        Connection connection = null;
        try {
            connection = getConnection();
            Message editmessagestext = new MessageDao().select(connection, messageId);
            commit(connection);

            return editmessagestext;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void update(Message message) {

    	Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().update(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}